import { DashboardComponent } from './shared/dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './usuarios/shared/auth.guard';

import { LoginLayoutComponent } from './layout/login-layout/login-layout.component';
import { LoginComponent } from './usuarios/login/login.component';
import { CriarContaComponent } from './usuarios/criar-conta/criar-conta.component';
import { EsqueciSenhaComponent } from './usuarios/esqueci-senha/esqueci-senha.component';

import { HomeLayoutComponent } from './layout/home-layout/home-layout.component';
import { FormCategoriaComponent } from './categorias/form-categoria/form-categoria.component';
import { ListaCategoriaComponent } from './categorias/lista-categoria/lista-categoria.component';
import { FormProdutoComponent } from './produtos/form-produto/form-produto.component';
import { ListaProdutoComponent } from './produtos/lista-produto/lista-produto.component';

const routes: Routes = [
  {
    path: '',
    component: HomeLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'categorias', component: ListaCategoriaComponent },
      { path: 'categorias/nova', component: FormCategoriaComponent },
      { path: 'categorias/editar/:key', component: FormCategoriaComponent },

      { path: 'produtos', component: ListaProdutoComponent },
      { path: 'produtos/novo', component: FormProdutoComponent },
      { path: 'produtos/editar/:key', component: FormProdutoComponent },

      { path: '', redirectTo: '/dashboard', pathMatch: 'full' }
    ]
  },
  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'criar-conta', component: CriarContaComponent },
      { path: 'esqueci-senha', component: EsqueciSenhaComponent }
    ]
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
