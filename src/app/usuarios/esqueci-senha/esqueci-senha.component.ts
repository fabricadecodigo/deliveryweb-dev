import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UsuarioService } from '../shared/usuario.service';

@Component({
  selector: 'app-esqueci-senha',
  templateUrl: './esqueci-senha.component.html',
  styleUrls: ['./esqueci-senha.component.scss']
})
export class EsqueciSenhaComponent implements OnInit {
  formEsqueciSenha: FormGroup;

  constructor(private formBuilder: FormBuilder, private usuarioService: UsuarioService, private toast: ToastrService) { }

  ngOnInit() {
    this.criarFormulario();
  }

  get email() { return this.formEsqueciSenha.get('email'); }

  criarFormulario() {
    this.formEsqueciSenha = this.formBuilder.group({
      email: ['contato@fabricadecodigo.com']
    });
  }

  onSubmit() {
    if (this.formEsqueciSenha.valid) {
      this.usuarioService.enviarEmailResetarSenha(this.formEsqueciSenha.value.email)
        .then(() => {
          this.toast.success('E-mail enviado com suceso');
        });
    }
  }
}
