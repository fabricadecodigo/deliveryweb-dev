import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private afAuth: AngularFireAuth) { }

  criarConta(usuario: any) {
    return  this.afAuth.auth.createUserWithEmailAndPassword(usuario.email, usuario.senha)
      .then((firebaseUser: firebase.auth.UserCredential) => {
        firebaseUser.user.updateProfile({ displayName: usuario.nome, photoURL: '' });
        firebaseUser.user.sendEmailVerification()
        this.logout();
      });
  }

  login(email: string, senha: string) {
    return new Promise((resolve, reject) => {
      this.afAuth.auth.signInWithEmailAndPassword(email, senha)
        .then((userCredential: firebase.auth.UserCredential) => {
          if (userCredential.user.emailVerified) {
            resolve();
          } else {
            this.logout();
            reject('Seu e-mail ainda não foi verificado. Por favor verifique seu e-mail.')
          }
        });
    });
  }

  logout() {
    return this.afAuth.auth.signOut();
  }

  enviarEmailResetarSenha(email: string) {
    return this.afAuth.auth.sendPasswordResetEmail(email);
  }
}
