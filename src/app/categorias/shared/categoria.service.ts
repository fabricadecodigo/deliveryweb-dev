import { FirebasePath } from './../../core/shared/firebase-path';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {
  categoriasRef: AngularFireList<any>;

  constructor(private db: AngularFireDatabase) {
    this.categoriasRef = this.db.list(FirebasePath.CATEGORIAS);
  }

  insert(categoria: any) {
    return this.categoriasRef.push(categoria);
  }

  update(categoria: any, key: string) {
    return this.categoriasRef.update(key, categoria)
      .then(() => {
        return this.updateProdutoRef(key, categoria.nome);
      });
  }

  getAll() {
    return this.categoriasRef.snapshotChanges().pipe(
      map(changes => {
        return changes.map(m => ({ key: m.payload.key, ...m.payload.val() }))
      })
    )
  }

  getByKey(key: string) {
    const path = `${FirebasePath.CATEGORIAS}${key}`;
    return this.db.object(path).snapshotChanges().pipe(
      map(change => {
        return ({ key: change.key, ...change.payload.val() });
      })
    );
  }

  remove(key: string) {
    return new Promise((resolve, reject) => {
      const subscribe = this.getProdutosByCategoria(key).subscribe((produtos: any) => {
        subscribe.unsubscribe();
        // Não remove se tem produto
        if (produtos.length == 0) {
          return this.categoriasRef.remove(key);
        } else {
          reject('A categoria tem produtos associados.')
        }
      })
    })
  }

  insertProdutoRef(categoriakey: string, produtoKey: string) {
    const categoriasProdutos = {};
    const path = `${FirebasePath.CATEGORIAS_PRODUTOS}${categoriakey}/${produtoKey}`;
    categoriasProdutos[path] = true;
    return this.db.object('/').update(categoriasProdutos);
  }

  updateProdutoRef(categoriakey: string, categoriaNome: string) {
    const subscribe = this.getProdutosByCategoria(categoriakey).subscribe((produtosKey: any) => {
      subscribe.unsubscribe();
      const produtosCategorias = {};

      produtosKey.forEach((produto: any) => {
        produtosCategorias[`${FirebasePath.PRODUTOS}/${produto.key}/categoriaNome`] = categoriaNome
      });

      return this.db.object('/').update(produtosCategorias);
    });
  }

  getProdutosByCategoria(key: string) {
    return this.db.list(`${FirebasePath.CATEGORIAS_PRODUTOS}${key}`).snapshotChanges().pipe(
      map(changes => {
        return changes.map(m => ({ key: m.key }))
      })
    )
  }

  removeProdutoRef(categoriakey: string, produtoKey: string) {
    const path = `${FirebasePath.CATEGORIAS_PRODUTOS}${categoriakey}/${produtoKey}`;
    return this.db.object(path).remove();
  }
}
