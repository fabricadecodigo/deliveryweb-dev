import { FirebasePath } from './../../core/shared/firebase-path';
import { CategoriaService } from './../../categorias/shared/categoria.service';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';


@Injectable({
  providedIn: 'root'
})
export class ProdutoService {
  produtosRef: AngularFireList<any>;

  constructor(private db: AngularFireDatabase, private categoriaService: CategoriaService) {
    this.produtosRef = this.db.list(FirebasePath.PRODUTOS);
  }

  insert(produto: any) {
    return this.produtosRef.push(produto).then((result: any) => {
      return this.categoriaService.insertProdutoRef(produto.categoriaKey, result.key);
    });
  }

  update(produto: any, key: string) {
    return this.produtosRef.update(key, produto);
  }

  getAll() {
    return this.produtosRef.snapshotChanges().pipe(
      map(changes => {
        return changes.map(m =>  ({ key: m.payload.key, ...m.payload.val() }))
      })
    )
  }

  getByKey(key: string) {
    const path = `${FirebasePath.PRODUTOS}${key}`;
    return this.db.object(path).snapshotChanges().pipe(
      map(change => {
        return ({ key: change.key, ...change.payload.val() });
      })
    );
  }

  remove(key: string) {
    const subscribe = this.getByKey(key).subscribe((produto: any) => {
      subscribe.unsubscribe();
      const categoriaKey = produto.categoriaKey;
      this.produtosRef.remove(key);
      this.categoriaService.removeProdutoRef(categoriaKey, key);
    })
  }
}
