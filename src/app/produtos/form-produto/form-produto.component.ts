import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProdutoService } from '../shared/produto.service';
import { Observable } from 'rxjs';
import { CategoriaService } from 'src/app/categorias/shared/categoria.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-form-produto',
  templateUrl: './form-produto.component.html',
  styleUrls: ['./form-produto.component.scss']
})
export class FormProdutoComponent implements OnInit {
  formProduto: FormGroup;
  key: string;
  categorias: Observable<any[]>;

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private produtoService: ProdutoService, private categoriaService: CategoriaService) { }

  ngOnInit() {
    this.criarFormulario();
    this.categorias = this.categoriaService.getAll();

    let key = this.route.snapshot.paramMap.get('key');
    if (key) {
      const subscribe = this.produtoService.getByKey(key).subscribe((produto: any) => {
        // mostrar sem isso para a pessoa entender
        subscribe.unsubscribe();

        this.key = produto.key;
        this.formProduto.setValue({
          nome: produto.nome,
          descricao: produto.descricao,
          preco: produto.preco,
          categoriaKey: produto.categoriaKey,
          categoriaNome: produto.categoriaNome
        });
      })
    }
  }

  get nome() { return this.formProduto.get('nome'); }
  get descricao() { return this.formProduto.get('descricao'); }
  get preco() { return this.formProduto.get('preco'); }
  get categoriaKey() { return this.formProduto.get('categoriaKey'); }
  get categoriaNome() { return this.formProduto.get('categoriaNome'); }

  criarFormulario() {
    this.key = null;
    this.formProduto = this.formBuilder.group({
      nome: ['', Validators.required],
      descricao: ['', Validators.required],
      preco: ['', Validators.required],
      categoriaKey: ['', Validators.required],
      categoriaNome: ['']
    });
  }

  setCategoriaNome(categorias: any) {
    if (categorias && this.formProduto.value.categoriaKey) {
      this.categoriaNome.setValue(categorias[0].text);
    } else {
      this.categoriaNome.setValue('');
    }
  }

  onSubmit() {
    if (this.formProduto.valid) {
      if (this.key) {
        this.produtoService.update(this.formProduto.value, this.key);
      } else {
        this.produtoService.insert(this.formProduto.value);
      }
      this.criarFormulario();
    }
  }
}
