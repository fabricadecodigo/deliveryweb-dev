import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/usuarios/shared/usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor(private usuarioService: UsuarioService, private router: Router) { }

  ngOnInit() {
  }

  sair() {
    this.usuarioService.logout()
      .then(() => {
        this.router.navigate(['/login']);
      })
  }

}
