import { UsuarioService } from './../../usuarios/shared/usuario.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(private usuarioService: UsuarioService, private router: Router) { }

  ngOnInit() {
  }

  sair() {
    this.usuarioService.logout()
      .then(() => {
        this.router.navigate(['/login']);
      })
  }
}
