export class FirebasePath {
  public static PRODUTOS: string = 'produtos/'
  public static CATEGORIAS: string = 'categorias/'
  public static CATEGORIAS_PRODUTOS: string = 'categoriasProdutos/'
}
