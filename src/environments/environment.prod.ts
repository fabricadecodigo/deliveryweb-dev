export const environment = {
  production: true,
  firebase: {
    apiKey: '<sua-key>',
    authDomain: '<seu-project-authdomain>',
    databaseURL: '<sua-database-URL>',
    projectId: '<seu-project-id>',
    storageBucket: '<seu-storage-bucket>',
    messagingSenderId: '<seu-messaging-sender-id>'
  }
};
